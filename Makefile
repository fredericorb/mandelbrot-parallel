CC=gcc
OUT=out
BIN=bin
C_FLAGS=-g -lpthread -lX11 -lm

build: $(BIN) queue printer logger
	$(CC) $(OUT)/*.o main.c $(C_FLAGS) -o $(BIN)/fractal

queue: $(OUT) queue/*.c
	$(CC) -c queue/*.c $(C_FLAGS) -o $(OUT)/queue.o

printer: $(OUT) printer/*.c
	$(CC) -c printer/*.c $(C_FLAGS) -o $(OUT)/printer.o

logger: $(OUT) logger/*.c
	$(CC) -c logger/*.c $(C_FLAGS) -o $(OUT)/logger.o


$(BIN):
	mkdir -p $(BIN)
$(OUT): 
	mkdir -p $(OUT)

.PHONY: build queue printer logger


