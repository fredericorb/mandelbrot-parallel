#ifndef LIBPRINTER_H
#define LIBPRINTER_H

#include <X11/Xlib.h> 

typedef struct {
	int xi, yi, xf, yf;
	int* points;
} print_section_t;

void init_screen(int width, int height, int max_colors);

void print_section(print_section_t* sec);

#endif
