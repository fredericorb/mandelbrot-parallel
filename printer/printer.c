#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "printer.h"

#define NIL (0) 
#define MAX_INT 65535

static Display *display; 
static Window window;
static GC gc;

static int width, height;
static int MAX_COLORS;

int blackColor, whiteColor;

Colormap colormap;
XColor red;
XColor *colors;

float line (float x, float x0, float x1, float y0, float y1) {
	return y0 + (y1 - y0) * (x - x0) / (x1 - x0);
}

float colorValue (float x, float x0, float x1) {
	return line(x, x0, x1, 0, MAX_INT);
}

void init_colors() {
	int screenNumber = DefaultScreen(display);
	colors = calloc(MAX_COLORS, sizeof(XColor));

	colormap = DefaultColormap(display, screenNumber);

	float logMax = logf((float)MAX_COLORS);

	for (int i = 0; i < MAX_COLORS; i++) {
		colors[i].red = colorValue(logf(i + 1.0f), 0, logMax);
		/*
		colors[i].green = colorValue(cos(i) + 1, 0, 2);
		*/
		colors[i].green = 2300 + ((i*100)%65535);
		colors[i].blue = 15000 + (i * 30);
		XAllocColor(display, colormap, &colors[i]);
	}

	red.red = 62000; red.green = 15000; red.blue = 2000;
	red.flags = DoRed | DoGreen | DoBlue;

	XAllocColor(display, colormap, &red);
}

void init_screen(int w, int h, int max_colors) {
	width = w;
	height = h;
	MAX_COLORS = max_colors;

	display = XOpenDisplay(NIL);

	assert (display);

	window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 
				 w, h, 0, blackColor, blackColor);

	XSelectInput(display, window, StructureNotifyMask);
	XMapWindow(display, window);
	for(;;) {
		XEvent e;
		XNextEvent(display, &e);
		if (e.type == MapNotify)
			break;
	}

	gc = XCreateGC(display, window, 0, NIL);

	init_colors();

	XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
	XSetBackground(display, gc, BlackPixel(display, DefaultScreen(display)));

	XFlush(display);
}

void print_section(print_section_t* sec) {
	// XSetForeground(display, gc, red.pixel);
	int sec_width = sec->xf - sec->xi;
	int sec_height = sec->yf - sec->yi;
	for (int i = 0; i < sec_width; i++) {
		for (int j = 0; j < sec_height; j++) {
			int point = sec->points[i * sec_height + j];
			if (point > 0) {
				XSetForeground(display, gc, colors[point].pixel);
				XDrawPoint(display, window, gc, (i + sec->xi), (j + sec->yi));
			}
		}
	}

	XFlush(display);
}

