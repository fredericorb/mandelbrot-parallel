#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "queue/queue.h"
#include "logger/logger.h"
#include "printer/printer.h"

queue_t *work, *result;

#define N_SLAVES 20
#define MAX_TASKS 999 
#define W_WIDTH 1000
#define W_HEIGHT 1000
#define WORK_SLICE 50
#define MAX_ITERATIONS 2000

void *master_thread ();
void *slave_thread ();
void *print_thread ();

int work_done = 0;

float scale = 1;
float dx = 0, dy = 0; 

pthread_mutex_t work_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t result_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
	int xi, xf, yi, yf;
} work_request_t;

int main(int argc, char* argv[]) {
	FILE *f = fopen("log.txt", "w"); 

	if (argc > 3) {
		scale = atof(argv[1]);
		dx = atof(argv[2]);
		dy = atof(argv[3]);
	}

	logger_initialize(f);
	logger_set_level(0);
	// logger_set_write_to_stdout(1);

	work = q_create(MAX_TASKS + 1);
	result = q_create(MAX_TASKS + 1);

	pthread_t master;

	int id_master = pthread_create(&master, NULL, master_thread, NULL);

	pthread_join(master, NULL);

	sleep(100);
}

void *master_thread () {
	int ch = logger_register_channel("master thread");
	pthread_t* slaves = calloc(N_SLAVES, sizeof(pthread_t));
	pthread_t print;

	for (int row = 0; row < W_HEIGHT / WORK_SLICE; row++) {
		for (int column = 0; column < W_WIDTH / WORK_SLICE; column++) {
			work_request_t* req = malloc(sizeof(work_request_t*));
			req->xi = (column * WORK_SLICE);
			req->yi = (row * WORK_SLICE);
			req->xf = ((column+1) * WORK_SLICE);
			req->yf = ((row+1) * WORK_SLICE);
			q_enqueue(work, req);
		}
	}

	int channels[N_SLAVES];
	for (int i = 0; i < N_SLAVES; i++) {
		char m[50];
		sprintf(m, "Starting thread (count: %d)", i);
		logger_info(ch, m);
		char str[15];
		sprintf(str, "thread (%d)", i);
		channels[i] = logger_register_channel(str);
		pthread_create(&slaves[i], NULL, slave_thread, (void*)channels[i]);
	}

	int id_print = pthread_create(&print, NULL, print_thread, NULL);

	for (int i = 0; i < N_SLAVES; i++) {
		pthread_join(slaves[i], NULL);
	}

	pthread_join(print, NULL);

	logger_finish();
}

void *slave_thread (void* arg) {
	while (work_done != 1) {
		int id = (int)arg;
		logger_info(id, "starting work");
		pthread_mutex_lock(&work_mutex);
		if (q_empty(work) == 1)  {
			logger_warn(id, "Empty work queue. Finishing");
			work_done = 1;
			pthread_mutex_unlock(&work_mutex);
			return;
		}
		work_request_t* problem = (work_request_t*)q_dequeue(work);
		pthread_mutex_unlock(&work_mutex);

		print_section_t* section = malloc(sizeof(print_section_t*));
		section->xi = problem->xi;
		section->xf = problem->xf;
		section->yi = problem->yi;
		section->yf = problem->yf;

		section->points = malloc(sizeof(int) * WORK_SLICE * WORK_SLICE);
		for (int col = section->xi; col < section->xf; col++) {
			for (int row = section->yi; row < section->yf; row++) {
				double c_re = ((col - W_WIDTH/2.0) * 4.0/W_WIDTH) * scale + dx;
				double c_im = ((row - W_HEIGHT/2.0) * 4.0/W_WIDTH) * scale + dy;
				double x = 0, y = 0;
				int iteration = 0;
				while (x*x + y*y <= 4 && iteration < MAX_ITERATIONS) {
					double x_new = x*x - y*y + c_re;
					y = 2*x*y + c_im;
					x = x_new;
					iteration++;
				}

				if (iteration < MAX_ITERATIONS) {
					section->points[(col - section->xi) * WORK_SLICE + (row - section->yi)] = iteration;
				}
			}
		}

		free(problem);
		pthread_mutex_lock(&result_mutex);
		q_enqueue(result, section);
		pthread_mutex_unlock(&result_mutex);
		char msg[50];
		sprintf(msg, "Result pushed to queue :: (%d,%d) - (%d,%d)", 
				section->xi, section->yi,
				section->xf, section->yf);
		logger_info(id, msg);
	}
}


void *print_thread () {
	int ch = logger_register_channel("printer");
	init_screen(W_WIDTH, W_HEIGHT, MAX_ITERATIONS);
	logger_warn(ch, "Initializing screen");
	pthread_mutex_lock(&result_mutex);
	int result_empty = q_empty(result);
	pthread_mutex_unlock(&result_mutex);
	while (work_done != 1 || result_empty != 1) {
		pthread_mutex_lock(&result_mutex);
		print_section_t* to_print = q_dequeue(result);
		result_empty = q_empty(result);
		pthread_mutex_unlock(&result_mutex);
		if (to_print != NULL) {
			logger_info(ch, "Printing");
			print_section(to_print);
		}
	}
}


















